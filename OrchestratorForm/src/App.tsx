import { Fragment, useState, useEffect } from 'react';
import { JsonForms } from '@jsonforms/react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import logo from './logo.svg';
import './App.css';
import schema from './schemas/configSchema.json';
import uischema from './schemas/configUISchema.json';
import {
  materialCells,
  materialRenderers,
} from '@jsonforms/material-renderers';
import RatingControl from './RatingControl';
import ratingControlTester from './ratingControlTester';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((_theme) => ({
  container: {
    padding: '1em',
    width: '100%'
  },
  buttonContainer: {
    textAlign: 'center'
  },
  title: {
    textAlign: 'center',
    padding: '0.50em',
  },
  dataContent: {
    display: 'flex',
    justifyContent: 'center',
    borderRadius: '0.25em',
    backgroundColor: '#cecece',
    marginBottom: '1rem',
  },
  resetButton: {
    margin: '5px',
    display: 'inline'
  },
  demoform: {
    margin: 'auto',
    padding: '1rem',
  },
}));

const initialData = {
  "operations": [
    {
      "type": "Run Bot",
      "name": "SalesforceBot",
      "path": "Automation Anywhere\\Bots\\Orchestrator\\Demo"
    },
    {
      "type": "Run Bot",
      "name": "WorkdayBot",
      "path": "Automation Anywhere\\Bots\\Orchestrator\\Demo"
    }
  ]
};

const renderers = [
  ...materialRenderers,
  //register custom renderers
  { tester: ratingControlTester, renderer: RatingControl },
];

const App = () => {
  const classes = useStyles();
  const [displayDataAsString, setDisplayDataAsString] = useState('');
  const [jsonformsData, setJsonformsData] = useState<any>(initialData);

  useEffect(() => {
    setDisplayDataAsString(JSON.stringify(jsonformsData, null, 2));
  }, [jsonformsData]);

  const clearData = () => {
    setJsonformsData({});
  };

  const saveFile = () => {
    const element = document.createElement("a");
    const file = new Blob([JSON.stringify(jsonformsData)], { type: 'text/json' });
    element.href = URL.createObjectURL(file);
    element.download = "config.json";
    document.body.appendChild(element); // Required for this to work in FireFox
    element.click();
  };

  return (
    <Fragment>
      <div className='App'>
        <header className='App-header'>
          <img src={logo} className='App-logo' alt='logo' />
          <h1 className='App-title'>Bot Orchestrator Config File Wizard</h1>
          <h2 className='App-intro'>Orchestrate bots with no code!</h2>
        </header>
      </div>

      <Grid
        container
        justify={'center'}
        spacing={1}
        className={classes.container}
      >
        <Grid item sm={8}>
          <Typography variant={'h5'} className={classes.title}>
            Config File Wizard
          </Typography>
          <div className={classes.demoform}>
            <JsonForms
              schema={schema}
              uischema={uischema}
              data={jsonformsData}
              renderers={renderers}
              cells={materialCells}
              onChange={({ errors, data }) => setJsonformsData(data)}
            />
          </div>
        </Grid>
        <Grid item sm={4}>
          <Typography variant={'h5'} className={classes.title}>
            Generated Config
          </Typography>
          <div className={classes.dataContent}>
            <pre id='boundData'>{displayDataAsString}</pre>
          </div>
          <div className={classes.buttonContainer}>
            <Button
              className={classes.resetButton}
              onClick={clearData}
              color='primary'
              variant='contained'
            >
              Clear data
            </Button>
            <Button
              className={classes.resetButton}
              onClick={saveFile}
              color='primary'
              variant='contained'
            >
              Save Config File
            </Button>
          </div>

        </Grid>

      </Grid>
    </Fragment>
  );
};

export default App;
